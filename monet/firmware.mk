# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi-firmware/monet/xbl.elf:install/firmware-update/xbl.elf \
    vendor/xiaomi-firmware/monet/xbl_config.elf:install/firmware-update/xbl_config.elf \
    vendor/xiaomi-firmware/monet/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi-firmware/monet/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi-firmware/monet/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi-firmware/monet/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi-firmware/monet/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi-firmware/monet/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi-firmware/monet/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi-firmware/monet/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi-firmware/monet/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi-firmware/monet/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi-firmware/monet/km4.mbn:install/firmware-update/km4.mbn \
    vendor/xiaomi-firmware/monet/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi-firmware/monet/featenabler.mbn:install/firmware-update/featenabler.mbn \
    vendor/xiaomi-firmware/monet/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi-firmware/monet/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi-firmware/monet/imagefv.elf:install/firmware-update/imagefv.elf \
    vendor/xiaomi-firmware/monet/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn